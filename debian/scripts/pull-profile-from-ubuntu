#!/bin/sh

set -e
set -u

NAME=$(basename ${0})
USAGE="Usage: $NAME SOURCE_PACKAGE --profile-name NAME [ OPTION... ] 
Options:
  --release RELEASE              extract the latest version of the source
                                 package from the specified release
  --version VERSION              extract the specified version of the source
                                 package
  --profile-source FILENAME      get the profile from the specified filename in
                                 the unpacked source package tree
  --profile-name NAME            install the profile under this name into
                                 /etc/apparmor.d/
  --abstraction-source FILENAME  get the abstraction from the specified filename
                                 in the unpacked source package tree
  --abstraction-name             install the profile under this name into
                                 /etc/apparmor.d/abstractions/
"

error() {
   echo "$@" >&2
   exit 1
}

### Parse and validate command-line

[ $# -ge 1 ] || error "$USAGE"

SOURCE_PACKAGE="$1" ; shift
[ -n "$SOURCE_PACKAGE" ] || error "$USAGE"

VERSION_SPECIFIER=
PROFILE_SOURCE=
PROFILE_NAME=
ABSTRACTION_SOURCE=
ABSTRACTION_NAME=

SHORTOPTS=
LONGOPTS="release:,version:"
LONGOPTS="$LONGOPTS,profile-source:,profile-name:"
LONGOPTS="$LONGOPTS,abstraction-source:,abstraction-name:"
OPTS=$(getopt -o $SHORTOPTS --longoptions $LONGOPTS -n "${NAME}" -- "$@")
eval set -- "$OPTS"
while [ $# -gt 0 ]; do
   case $1 in
      --release|--version)
	 shift
	 VERSION_SPECIFIER="$1"
	 ;;
      --profile-source)
	 shift
	 PROFILE_SOURCE="$1"
	 ;;
      --profile-name)
	 shift
	 PROFILE_NAME="$1"
	 ;;
      --abstraction-source)
	 shift
	 ABSTRACTION_SOURCE="$1"
	 ;;
      --abstraction-name)
	 shift
	 ABSTRACTION_NAME="$1"
	 ;;
      --help)
	 echo "$USAGE" >&2
	 exit 0
	 ;;
   esac
   shift
done

# Options consistency checks
[ -n "$PROFILE_NAME" ]   || error "--profile-name is required.\n$USAGE"
if [ -n "$ABSTRACTION_SOURCE" ]; then
   [ -n "$ABSTRACTION_NAME" ] || \
      error "--abstraction-name is required when --abstraction-source is used."
fi

# Fallback settings
[ -n "$PROFILE_SOURCE" ] || PROFILE_SOURCE=debian/apparmor-profile
if [ -n "$ABSTRACTION_NAME" ] && [ -z "$ABSTRACTION_SOURCE" ]; then
   ABSTRACTION_SOURCE=debian/apparmor-profile.abstraction
fi

   
### Initialization and sanity checks

[ -x "$(which pull-lp-source)" ] || \
   error "pull-lp-source not found. Install the ubuntu-dev-tools package."

TEMP_DIR=$(mktemp -d)
WORK_DIR=$(readlink -f $(dirname $(dirname $(dirname "$0"))))
PROFILES_DIR="$WORK_DIR/profiles"
ABSTRACTIONS_DIR="$PROFILES_DIR/abstractions"

[ -d "$PROFILES_DIR" ]     || error "'$PROFILES_DIR' is not a directory."
[ -d "$ABSTRACTIONS_DIR" ] || error "'$ABSTRACTIONS_DIR' is not a directory."

### Main

(
   cd "$TEMP_DIR"
   if [ -n "$VERSION_SPECIFIER" ]; then
      pull-lp-source "$SOURCE_PACKAGE" "$VERSION_SPECIFIER"
   else
      pull-lp-source "$SOURCE_PACKAGE"
   fi

   cp "$SOURCE_PACKAGE"*/"$PROFILE_SOURCE" "$PROFILES_DIR/$PROFILE_NAME"
   if [ -n "$ABSTRACTION_NAME" ]; then
      cp "$SOURCE_PACKAGE"*/"$ABSTRACTION_SOURCE" \
	 "$ABSTRACTIONS_DIR/$ABSTRACTION_NAME"
   fi
)
