apparmor-profiles-extra (1.19) unstable; urgency=medium

  * Totem: update to the latest version taken from the upstream
    apparmor-profiles repository at commit 55ba139

 -- intrigeri <intrigeri@debian.org>  Mon, 15 Jan 2018 08:51:41 +0000

apparmor-profiles-extra (1.18) unstable; urgency=medium

  * Update Vcs-* control fields for Salsa.
  * Configure gbp.conf for DEP-14.

 -- intrigeri <intrigeri@debian.org>  Sun, 14 Jan 2018 12:57:50 +0000

apparmor-profiles-extra (1.17) unstable; urgency=medium

  * Pidgin: update to the latest version taken from the upstream
    apparmor-profiles repository at commit 55ba139 (Closes: #884787).
    Thanks to Adrian Heine <debian@adrianheine.de> for the patch!
  * Bump debhelper compat level to 11, adjust build-dependencies accordingly.
  * Bump Standards-Version to 4.1.3 (no change needed).

 -- intrigeri <intrigeri@debian.org>  Sun, 07 Jan 2018 12:33:52 +0000

apparmor-profiles-extra (1.16) unstable; urgency=medium

  * Totem: update to latest upstream profile; fixes segfault
    when using the NVIDIA proprietary driver (Closes: #879900).

 -- intrigeri <intrigeri@debian.org>  Mon, 30 Oct 2017 08:39:46 +0000

apparmor-profiles-extra (1.15) unstable; urgency=medium

  [ Vincas Dargis ]
  * Totem: import patch from updated merge request to fix bwrap rule qualifier
    (Closes: #877255).

  [ intrigeri ]
  * Set priority to "optional": "extra" is deprecated.
  * Bump Standards-Version to 4.1.1 (no change needed).
  * Totem, gstreamer abstraction, gst_plugin_scanner: update to work better
    on GNOME 3.26 and Linux 4.14-rc5.

 -- intrigeri <intrigeri@debian.org>  Wed, 25 Oct 2017 08:55:09 +0000

apparmor-profiles-extra (1.14) unstable; urgency=medium

  * Totem: import fixes (submitted upstream) for 3.26.
  * Update README.Debian.

 -- intrigeri <intrigeri@debian.org>  Wed, 20 Sep 2017 15:48:06 +0000

apparmor-profiles-extra (1.13) unstable; urgency=medium

  * Add a script allowing the source package to put specific profiles
    in complain mode.
  * Put the irssi profile in complain mode (Closes: #866792).
  * Totem: update to the version from the apparmor-profiles repository
    at commit bfc0bff (Closes: #867692).

 -- intrigeri <intrigeri@debian.org>  Sat, 09 Sep 2017 21:24:02 +0000

apparmor-profiles-extra (1.12) unstable; urgency=medium

  * Remove tcpdump profile: it's shipped by the tcpdump package >= 4.9.0-3.
  * gst_plugin_scanner: update to version from current apparmor-profiles
    repo (at commit 5ba92ee).
  * README.Debian: refresh status of all profiles.
  * Totem: update to
    ~intrigeri/apparmor-profiles/+git/apparmor-profiles/+merge/310120.
  * Declare compliance with Standards-Version 4.0.0 (no change required).
  * Bump debhelper compat level to 10, adjust build-dependencies accordingly.
  * Update copyright years.

 -- intrigeri <intrigeri@debian.org>  Mon, 03 Jul 2017 07:13:34 +0000

apparmor-profiles-extra (1.11) unstable; urgency=medium

  * apt-cacher-ng, irssi, pidgin: update from the apparmor-profiles repository
    at commit 392d8ab, for compatibility with merged-/usr.
  * tcpdump: update from Ubuntu, and apply patch submitted to them
    (https://bugs.launchpad.net/ubuntu/+source/tcpdump/+bug/1647188),
    for compatibility with merged-/usr.
  * README.Debian: update status of every profile.
  * Improve long description.
  * README.source: document how to update the tcpdump profile.
  * Update URL of upstream apparmor-profiles repo, that has moved to Git.

 -- intrigeri <intrigeri@debian.org>  Fri, 06 Jan 2017 09:00:08 +0000

apparmor-profiles-extra (1.10) unstable; urgency=medium

  * Totem profile: add udev rules needed under current sid.

 -- intrigeri <intrigeri@debian.org>  Fri, 29 Jul 2016 08:50:17 +0000

apparmor-profiles-extra (1.9) unstable; urgency=medium

  * apt-cacher-ng profile: update from the apparmor-profiles repository
    at revision 169.
  * Totem profiles: update from the apparmor-profiles repository
    at revision 165. Thanks to Julian Andres Klode <jak@debian.org>
    for the initial patch! (Closes: #808859)
  * Remove Holger from Uploaders, at his request; thanks for the work
    you did on this package! (Closes: #824462)
  * README.Debian: update freshness information for all profiles.

 -- intrigeri <intrigeri@debian.org>  Fri, 24 Jun 2016 16:32:32 +0000

apparmor-profiles-extra (1.8) unstable; urgency=medium

  * Drop ntpd profile and tunable: they are shipped by the ntp
    package starting with 1:4.2.8p7+dfsg-1 (Closes: #768415, #799084).

 -- intrigeri <intrigeri@debian.org>  Sat, 30 Apr 2016 10:52:46 +0000

apparmor-profiles-extra (1.7) unstable; urgency=medium

  [ Felix Geyer ]
  * Make build reproducible.
  * Use https for Vcs-Git URL.
  * Update ntpd profile from the latest Ubuntu package.

  [ intrigeri ]
  * Drop Evince profile and abstraction: they are shipped
    by the evince package starting with 3.20.0-2.
  * Declare compliance with Standards-Version 3.9.8.

 -- intrigeri <intrigeri@debian.org>  Thu, 28 Apr 2016 09:22:34 +0000

apparmor-profiles-extra (1.6) unstable; urgency=medium

  * apt-cacher-ng profile: update from the apparmor-profiles repository
    at revision 153: Allow apt-cacher-ng to run acngtool
    (with inherited policy).
  * Pidgin profile: support paths used by recent GStreamer, and cases
    when the registry has not been initialized yet (Closes: #802791).
  * Totem profile: update from the apparmor-profiles repository
    at revision 153: add raw device access to DVD readers.

 -- intrigeri <intrigeri@debian.org>  Sat, 14 Nov 2015 13:38:42 +0000

apparmor-profiles-extra (1.5) unstable; urgency=medium

  * Update GStreamer abstraction to work with /tmp mounted noexec.
  * Update Pidgin profile from the apparmor-profiles repository at revision 146:
    - allow only read-only access to DConf
    - some cleanup
    - use GStreamer abstraction
    - allow reading global libpurple preferences
    - support the blinklight plugin
  * README.Debian: update profiles status -- all our changes went upstream.

 -- intrigeri <intrigeri@debian.org>  Thu, 27 Aug 2015 12:13:30 +0200

apparmor-profiles-extra (1.4) unstable; urgency=medium

  [ Holger Levsen ]
  * debian/control: Use new cgit URL for Vcs-Browser. Wrap some lines.
    Thanks to 'cme fix dpkg' for the patch.

  [ intrigeri ]
  * Version the {build,runtime}-dependency on apparmor to >= 2.9.0.
  * Refresh Evince profile from Ubuntu, and this time leave
    the 2.9-specific includes in.
  * Pidgin profile: drop inlining of rule that's now part of
    the audio abstraction.
  * Totem profile:
    - drop inlining of rules that are now part of the dconf abstraction
    - allow access to the at-spi2 files and directories
  * Fix logic to make pull-profile-from-ubuntu work again.
  * debian/rules: use absolute path to apparmor_parser (Closes: #764726).

 -- intrigeri <intrigeri@debian.org>  Sun, 19 Oct 2014 10:36:13 +0200

apparmor-profiles-extra (1.3) unstable; urgency=medium

  * Add profile for apt-cacher-ng: taken from the apparmor-profiles
    repository at revision 135.
  * Update README.Debian wrt. the Pidgin profile modification.
  * Add missing local override include to the irssi, totem
    and totem-previewers profiles.
  * Declare compliance with Standards-Version 3.9.6 (no change needed).

 -- intrigeri <intrigeri@debian.org>  Wed, 15 Oct 2014 01:06:28 +0200

apparmor-profiles-extra (1.2) unstable; urgency=medium

  [ intrigeri ]
  * Reassign maintenance and copyright to the new united Debian AppArmor Team.
  * Allow Pidgin to read /etc/wildmidi/wildmidi.cfg.

  [ Felix Geyer ]
  * Add support for installing tunables.
  * Add a profile for ntpd.
  * Test that apparmor is able to parse the profiles.
  * Simplify profile installation process.
  * Include tunables/global in gst_plugin_scanner profile.
  * Inline abstractions/dconf in totem profile.

 -- intrigeri <intrigeri@debian.org>  Wed, 10 Sep 2014 14:54:09 -0700

apparmor-profiles-extra (1.1) unstable; urgency=medium

  [ Holger Levsen ]
  * New profile: irssi

  [ intrigeri ]
  * Evince profile:
    - refresh from the Ubuntu package (3.10.3-0ubuntu16, bzr r205)
    - comment-out the inclusion of the dbus-accessibility abstraction:
      it's not shipped by the version of AppArmor we have in Debian
  * tcpdump profile: refresh from the Ubuntu package 4.5.1-2ubuntu2 (bzr r33).
  * New profiles:
    - GStreamer abstraction
    - gst_plugin_scanner
    - Totem
  * Assign copyright on debian/* to the team.
  * Relicense debian/* under GPL-2+, for consistency with upstream.
  * Add copyright and license information for all profiles.

 -- intrigeri <intrigeri@debian.org>  Thu, 28 Aug 2014 09:03:58 -0700

apparmor-profiles-extra (1.0) unstable; urgency=medium

  [ intrigeri ]
  * Initial release.

 -- Holger Levsen <holger@debian.org>  Sat, 14 Jun 2014 16:54:34 +0200
